package com.leewise.companymanagerbyage.controller;

import com.leewise.companymanagerbyage.model.AgeItem;
import com.leewise.companymanagerbyage.model.AgeManageRequest;
import com.leewise.companymanagerbyage.service.AgeManageService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/manage")
@RequiredArgsConstructor
public class AgeManageController {
    private final AgeManageService ageManageService;
    @PostMapping("/data")
    public String setAgeManage(@RequestBody AgeManageRequest request) {
        ageManageService.setAgeManage(request.getProductName(), request.getGender(), request.getAge());
        return "Thanks";
    }
    @GetMapping("/agemanage")
    public List<AgeItem> getAge()  {
        List<AgeItem> result = ageManageService.getAges();
        return  result;
    }

}
