package com.leewise.companymanagerbyage.repository;

import com.leewise.companymanagerbyage.entity.PeopleAge;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyManageRepository extends JpaRepository<PeopleAge,Long> {
}
