package com.leewise.companymanagerbyage.service;

import com.leewise.companymanagerbyage.entity.PeopleAge;
import com.leewise.companymanagerbyage.model.AgeItem;
import com.leewise.companymanagerbyage.repository.CompanyManageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AgeManageService {
    private final CompanyManageRepository companyManageRepository;

    public void setAgeManage( String productName, String gender , Integer age ) {
        PeopleAge addData = new PeopleAge();
        addData.setGender(gender);
        addData.setAge(age);
        addData.setProductName(productName);

        companyManageRepository.save(addData);
    }
    public List<AgeItem> getAges() {
        List<AgeItem> result = new LinkedList<>();
        List<PeopleAge> originData = companyManageRepository.findAll();
        for(PeopleAge item : originData) {
            AgeItem addItem = new AgeItem();
            addItem.setAge(item.getAge());
            addItem.setProductName(item.getProductName());

            result.add(addItem);

        }
        return result;
    }
}
