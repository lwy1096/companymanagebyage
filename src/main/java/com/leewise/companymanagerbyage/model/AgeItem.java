package com.leewise.companymanagerbyage.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AgeItem {
    private Integer age;
    private String productName;

}
