package com.leewise.companymanagerbyage.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AgeManageRequest {
    private String productName;

    private Integer age;

    private String gender;
}
