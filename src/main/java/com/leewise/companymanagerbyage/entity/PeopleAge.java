package com.leewise.companymanagerbyage.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class PeopleAge {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 5)
    private Integer age;

    @Column(nullable = false, length = 30)
    private String productName;

    @Column(nullable = false,length = 30)
    private String gender;
}
