package com.leewise.companymanagerbyage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompanyManagerByAgeApplication {

    public static void main(String[] args) {
        SpringApplication.run(CompanyManagerByAgeApplication.class, args);
    }

}
